---
title: "Terms of Use"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle:
include_footer: true
---

Senofi Inc. (“Senofi”, “we” or “us”) owns and operates the website located at [consortia.io] (https://consortia.io). These terms of use (these “Terms”) apply to all users of the Website, including users who upload any materials to the Website, users who use services provided through the Website and users who simply view the content on or available through the Website.   

BY USING THIS WEBSITE, YOU INDICATE YOUR ACCEPTANCE OF THESE TERMS. IF YOU DO NOT ACCEPT THESE TERMS, THEN DO NOT USE THIS WEBSITE OR ANY OF ITS CONTENT OR SERVICES.  THESE TERMS MAY BE AMENDED OR UPDATED BY SENOFI FROM TIME TO TIME WITHOUT NOTICE AND THE TERMS MAY HAVE CHANGED SINCE YOUR LAST VISIT TO THIS WEBSITE.  IT IS YOUR RESPONSIBILITY TO REVIEW THESE TERMS FOR ANY CHANGES.  YOUR USE AFTER ANY AMENDMENTS OR UPDATES OF THESE TERMS SHALL SIGNIFY YOUR ASSENT TO AND ACCEPTANCE OF SUCH REVISED TERMS.  ANY NEW FEATURES THAT MAY BE ADDED TO THIS WEBSITE FROM TIME TO TIME WILL BE SUBJECT TO THESE TERMS, UNLESS STATED OTHERWISE.  YOU SHOULD VISIT THIS PAGE PERIODICALLY TO REVIEW THESE TERMS.

### 1. The Service

The Senofi platform allows individuals and organizations to create networks with other user groups to collaborate through the Website (the “Service”).  In order to access the Service, you must be a registered user.

### 2. Content

All information, data, text, software, music, sound, photographs, graphics, video, messages or other materials, whether publicly posted or privately transmitted to the Website by viewers or users whether in forums or otherwise (“User Content”), is the sole responsibility of such viewers or users. This means that the viewer or user, and not Senofi, are entirely responsible for all such material uploaded, posted, emailed, transmitted or otherwise made available by using the Service. Senofi does not control or actively monitor User Content and, as such, does not guarantee the accuracy, integrity or quality of such content. Users acknowledge that by using the Service, they may be exposed to materials that are offensive, indecent or objectionable.  Under no circumstances will Senofi be liable in any way for any materials, including, but not limited to, for any errors or omissions in any materials or any defects or errors in any printing or manufacturing, or for any loss or damage of any kind incurred as a result of the viewing or use of any materials posted, emailed, transmitted or otherwise made available via the Service.

### 3. Restrictions on User Content and Use of the Service

Senofi reserves the right at all times (but will have no obligation) to terminate users or reclaim usernames. We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce these Terms, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect the rights, property or safety of our users and the public.   

In using the Website and/or Service, you shall not:

__a.__ copy any content unless expressly permitted to do so herein;  

__b.__ upload, post, email, transmit or otherwise make available any material that:  

&nbsp;&nbsp;&nbsp;&nbsp;__i.__ is unlawful, harmful, threatening, abusive, harassing, tortuous, defamatory, vulgar, obscene, pornographic, libelous, invasive of another's privacy, hateful, or racially or ethnically objectionable, encourages criminal behavior, gives rise to civil liability, violates any law, or is otherwise objectionable;  
&nbsp;&nbsp;&nbsp;&nbsp;__ii.__ you do not have a right to make available under any law or under a contractual relationship;  
&nbsp;&nbsp;&nbsp;&nbsp;__iii.__ infringes any patent, trademark, trade secret, copyright or other proprietary rights of any party (including privacy rights);  
&nbsp;&nbsp;&nbsp;&nbsp;__iv.__ is or contains unsolicited or unauthorized advertising, solicitations for business, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other form of solicitation;  
&nbsp;&nbsp;&nbsp;&nbsp;__v.__ contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment or data or the Website or that of any users or viewers of the Website or that compromises a user’s privacy;  or  
&nbsp;&nbsp;&nbsp;&nbsp;__vi.__ contains any falsehoods or misrepresentations or create an impression that you know is incorrect, misleading, or deceptive, or any material that could damage or harm minors in any way;  

__c.__ stress test or load test any part of the Website and/or Service;  

__d.__ impersonate any person or entity or misrepresent their affiliation with a person or entity;  

__e.__ forge headers or otherwise manipulate identifiers in order to disguise the origin of any material transmitted to or through the Website or impersonate another person or organization;  

__f.__ interfere with or disrupt the Website or servers or networks connected to the Website, or disobey any requirements, procedures, policies or regulations of networks connected to the Website or probe, scan, or test the vulnerability of any system or network or breach or circumvent any security or authentication measures;  

__g.__ intentionally or unintentionally violate any applicable local, state, national or international law or regulation;  

__h.__ license, sell, rent, lease, transfer, assign, distribute, host, or otherwise commercially exploit the Website; or  

__i.__ modify, translate, make derivative works of, disassemble, decompile, reverse compile or reverse engineer any part of any software provided as part of the Website, except to the extent the foregoing restrictions are expressly prohibited by applicable law.  

### 4. License of Content

By submitting, posting or displaying User Content on or through the Service, you grant us (and our agents) a non-exclusive, royalty-free license (with the right to sublicense) to use, copy, modify, transmit, display and distribute such User Content.  Senofi will not be responsible or liable for any use of User Content in accordance with these Terms. You represent and warrant that you have all the rights, power and authority necessary to grant the rights granted herein to any User Content that you submit.

### 5. End User License

Except for User Content, this Website, and the information and materials that it contains, are the property of Senofi and its licensors, and are protected from unauthorized copying and dissemination by copyright law, trademark law, and other intellectual property laws.  Subject to the terms of these Terms, Senofi grants you a non-transferable, non-exclusive, license to use the Website for your use (the “License”). Nothing in these Terms gives you a right to use the Senofi names, trademarks, logos, domain names, and other distinctive brand features without our prior written consent.  You shall not attempt to override or circumvent any of the usage rules or restrictions on the Website.  Any future release, update, or other addition to functionality of the Website shall be subject to the terms of these Terms.

### 6. Payment Terms 
#### a. Fees. 
Senofi may charge fees for advanced support and maintenance to use the Service, which may be purchased for monthly or annual subscription terms.

#### b. Termination.
Unless otherwise stated or agreed to by Senofi in its sole and absolute discretion, you may terminate your Service subscription upon providing Senofi with written notice at least thirty (30) days prior to the end of the applicable subscription term.  You may provide us with notice by contacting us at [Insert Email Address].

#### c. Payment and Charges.
Your access to and use of the Service may be subject to your payment of the applicable fees due for the Service selected by you ("Fees") set out on the Website and all other applicable amounts, charges and taxes indicated to you when you purchase a subscription for the Service (or otherwise notified to you by Senofi from time to time) when you use the Service. Unless otherwise stated, all invoiced amounts are due upon receipt and processed immediately using the credit card on file for you. 

#### d. Billing Information.
In order to register for the Service, you may be required to provide valid credit card and billing information (“Billing Information”). All prices and invoices are in U.S. Dollars and you must pay in U.S. Dollars. Senofi uses third party payment processors (“Payment Processors”) to process transactions. You authorize your Billing Information to be provided to the third party Payment Processors. You acknowledge and agree that the terms of service of the Payment Processor will govern your agreement and interactions with the Payment Processor and that our Terms and policies do not govern. Senofi HAS NO LIABILITY ARISING FROM YOUR USE OF OR ACCESS TO THE PAYMENT PROCESSORS. You shall: (i) keep the Billing Information you provide to Senofi or its payment processors, including name, credit card number and expiry date, mailing address, email address and telephone number, accurate and up to date; otherwise, we may suspend the Service; (ii) promptly advise Senofi if your credit card information changes due to loss, theft, cancellation or otherwise; (iii) be liable for your failure to pay any Fees billed to you by Senofi caused by your failure to provide Senofi with up to date billing information.

#### e. Trial.
From time to time, Senofi may make available a free trial to use the Service for a specified period.

#### f. Refunds.
Senofi generally does not provide refunds for cancelled Service subscriptions, but will consider refunds on a case-by-case basis.

### 7. Feedback 

If you provide Senofi with any suggestions, comments or other feedback relating to any aspect of the Website and/or Service ("Feedback"), Senofi may use such Feedback in the Website or in any other Senofi products or services (collectively, "Senofi Offerings"). Accordingly, You agree that: (a) Senofi is not subject to any confidentiality obligations in respect to the Feedback, (b) the Feedback is not confidential or proprietary information of you or any third party and you have all of the necessary rights to disclose the Feedback to Senofi, (c) Senofi (including all of its successors and assigns and any successors and assigns of any of the Senofi Offerings) may freely use, reproduce, publicize, license, distribute, and otherwise commercialize Feedback in any Senofi Offerings, and (d) you are not entitled to receive any compensation or re-imbursement of any kind from Senofi or any of the other users of the Website in respect of the Feedback.

### 8. Advertising

You acknowledge and agree that the Website may contain advertisements.  If you elect to have any business dealings with anyone whose products or services may be advertised on the Website, you acknowledge and agree that such dealings are solely between you and such advertiser and you further acknowledge and agree that Senofi shall not have any responsibility or liability for any losses or damages that You may incur as a result of any such dealings.  You shall be responsible for obtaining access to the Website and acknowledge that such access may involve third-party fees (such as Internet service provider access or data fees).  You shall be solely responsible for any such fees and also for obtaining any equipment that is required to access the Website.  It is your responsibility to ascertain whether any information or materials downloaded from the Website are free of viruses, worms, Trojan Horses, or other items of a potentially destructive nature.

### 9. Links and Third-Party Websites

This Website (including User Content) may contain links to other websites that are not owned or controlled by Senofi.  In no event shall any reference to any third party, third party product or service be construed as an approval or endorsement by Senofi of that third party, third party product or service.  Senofi is also not responsible for the content of any linked websites.  Any third-party websites or services accessed from the Website are subject to the terms and conditions of those websites and or services and you are responsible for determining those terms and conditions and complying with them.  The presence on the Website of a link to any other website(s) does not imply that Senofi endorses or accepts any responsibility for the content or use of such websites, and you hereby release Senofi from all liability and damages that may arise from your use of such websites or receipt of services from any such websites.

### 10. DISCLAIMER OF REPRESENTATIONS, WARRANTIES AND CONDITIONS 

THE WEBSITE, SERVICE AND ALL MATERIALS PROVIDED THEREIN ARE PROVIDED "AS IS."  Senofi SPECIFICALLY DISCLAIMS ALL REPRESENTATIONS, WARRANTIES AND CONDITIONS, EITHER EXPRESS, IMPLIED, STATUTORY, BY USAGE OF TRADE, COURSE OF DEALING OR OTHERWISE INCLUDING BUT NOT LIMITED TO ANY IMPLIED WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, TITLE, SATISFACTORY QUALITY OR FITNESS FOR A PARTICULAR PURPOSE.  ANY INFORMATION OR MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICE IS AT YOUR OWN DISCRETION AND RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM, LOSS OF DATA, OR ANY OTHER LOSS THAT RESULTS FROM DOWNLOADING OR USING ANY SUCH MATERIAL.  Senofi DOES NOT WARRANT, ENDORSE, GUARANTEE, PROVIDE ANY CONDITIONS OR REPRESENTATIONS, OR ASSUME ANY RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY ANY THIRD PARTY THROUGH THE WEBSITE OR IN RESPECT TO ANY WEBSITE THAT CAN BE REACHED FROM A LINK ON THE WEBSITE OR FEATURED IN ANY BANNER OR OTHER ADVERTISING ON THE WEBSITE, AND Senofi SHALL NOT BE A PARTY TO ANY TRANSACTION THAT YOU MAY ENTER INTO WITH ANY SUCH THIRD PARTY.  Senofi WILL NOT BE LIABLE FOR ANY TYPE OF CONTENT EXCHANGED BY MEANS OF THE SERVICE. 

### 11. LIMITATION OF LIABILITY

UNDER NO CIRCUMSTANCES SHALL Senofi BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, EXEMPLARY OR OTHER DAMAGES WHATSOEVER, INCLUDING, WITHOUT LIMITATION, ANY DAMAGES THAT RESULT FROM (I) YOUR USE OF OR YOUR INABILITY TO USE THIS WEBSITE OR THE SERVICE, including, without limitation, due to downtime For installations, updates or any other actions on our equipment in order to provide the Service and maintain the Website, (II) THE COST OF PROCUREMENT OF SUBSTITUTE GOODS, DATA, INFORMATION OR SERVICES, (III) ERRORS, MISTAKES, OR INACCURACIES ON THE WEBSITE, (IV) PERSONAL INJURY OR PROPERTY DAMAGE OF ANY KIND WHATSOEVER ARISING FROM OR RELATING TO YOUR USE OF THE SERVICE, ANY BUGS, VIRUSES, TROJAN HORSES, OR ANY OTHER FILES OR DATA THAT MAY BE HARMFUL TO COMPUTER OR COMMUNICATION EQUIPMENT OR DATA THAT MAY HAVE BEEN TRANSMITTED TO OR THROUGH THE WEBSITE, (V) any security breaches to user-provided equipment, OR (VI) ANY ERRORS OR OMISSIONS IN ANY MATERIAL ON THE WEBSITE OR ANY OTHER LOSS OR DAMAGE OF ANY KIND ARISING FROM OR RELATING TO YOUR USE OF THE WEBSITE.  THESE LIMITATIONS SHALL APPLY EVEN IF Senofi HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, Senofi’S LIABILITY TO YOU FOR ANY DAMAGES ARISING FROM OR RELATED TO THIS AGREEMENT (FOR ANY CAUSE WHATSOEVER AND REGARDLESS OF THE FORM OF THE ACTION), WILL AT ALL TIMES BE LIMITED TO THE GREATER OF (A) FIFTY Canadian DOLLARS ($50 CAD) OR (B) AMOUNTS YOU’VE PAID SENOFI IN THE PRIOR 12 MONTHS (IF ANY).  THE FOREGOING LIMITATIONS SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION. 

### 12. INDEMNIFICATION

YOU SHALL INDEMNIFY AND HOLD Senofi AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, AGENTS, AND EMPLOYEES, HARMLESS FROM ALL CLAIMS, ACTIONS, PROCEEDINGS, DEMANDS, DAMAGES, LOSSES, COSTS, AND EXPENSES (INCLUDING REASONABLE ATTORNEYS' FEES), INCURRED IN CONNECTION WITH ANY MATERIALS SUBMITTED, POSTED, TRANSMITTED OR MADE AVAILABLE BY YOU THROUGH THE SERVICE AND/OR ANY VIOLATION BY YOU OF THESE TERMS.

### 13. Termination

Senofi may, under certain circumstances and without prior notice, immediately terminate your ability to access the Website or portions thereof. Cause for such termination shall include, but not be limited to, (a) breaches or violations of these Terms or any other agreement that you may have with Senofi (including, without limitation, non-payment of any fees owed in connection with the Service or otherwise owed by you to Senofi), (b) requests by law enforcement or other government agencies, (c) a request by you, (d) discontinuance or material modification to the Website (or any part thereof), (e) unexpected technical, security or legal issues or problems, and/or (f) participation by you, directly or indirectly, in fraudulent or illegal activities.  Termination of your access to the Website may also include removal of some or all of the materials uploaded by you to the Website.  You acknowledge and agree that all terminations may be made by Senofi in its sole discretion and that Senofi shall not be liable to you or any third-party for any termination of your access to this Website or for the removal of any of the materials uploaded by you to the Website.  Any termination of these terms of use by Senofi shall be in addition to any and all other rights and remedies that Senofi may have.

### 14. Availability and Updates
Senofi may alter, suspend, or discontinue this Website at any time and for any reason or no reason, without notice. The Website and/or Service may be unavailable from time to time due to maintenance or malfunction of computer or network equipment or other reasons.  Senofi may periodically add or update the information and materials on this Website without notice.    
### 15. Security

Information sent or received over the Internet is generally unsecure and Senofi cannot and does not make any representation or warranty concerning security of any communication to or from the Website or any representation or warranty regarding the interception by third parties of personal or other information.   You are responsible for safeguarding the password that you use to access the Service and you are responsible for any activities or actions under your password. You agree to keep your password secure.  Senofi will not be liable for any loss or damage arising from your failure to comply with these requirements.

### 16. General 

These Terms, together with any privacy policy that may be published on the Website, constitutes the entire agreement between the parties relating to the Website and Service and all related activities.  These Terms shall not be modified except in writing signed by both parties or by a new posting of these Terms issued by Senofi.  If any part of these Terms is held to be unlawful, void, or unenforceable, then that part shall be deemed severed and shall not affect the validity and enforceability of the remaining provisions.  The failure of Senofi to exercise or enforce any right or provision under these Terms shall not constitute a waiver of such right or provision.  Any waiver of any right or provision by Senofi must be in writing and shall only apply to the specific instance identified in such writing.  You may not assign these Terms, or any rights or licenses granted hereunder, whether voluntarily, by operation of law, or otherwise without Senofi’s prior written consent. 

If you have any questions about these Terms, please contact us at [info@senofi.ca](mailto:info@senofi.ca).
</br></br>

Last Updated: 1 July, 2020.
