---
title: "Frequently Asked Questions"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: Find you answer below and don't hesitate to contact us.
include_footer: true
---

#### What is the software used to manage the deployments of the nodes?
We support [Docker Swarm](https://docs.docker.com/engine/swarm/). We are planning to support [Kubernetes (k8s)](https://kubernetes.io/) soon, so stay tuned.


#### What OS is supported on the machines?
Ubuntu 18 and 16. We plan to extend the support to other OS in the future.

#### What protocol is used to manage the machines, nodes and applications?
The machines and related software are setup and configured using SSH.


#### What Cloud providers I can use to provision the machines?
Any cloud provider. Consortia takes care to setup all the software needed to run the network. This is done in order to keep the solution cloud agnostic.

#### What Hyperledger Fabric version is supported?
Hyperledger Fabric 1.4.9 (LTS), 2.2.1 (LTS) and 2.3.0. Keep checking this page for future updates as we plan to support more upcoming community releases.

#### What Certificate Authority (CA) server is used?
Hyperledger CA. You can find details here: https://hyperledger-fabric-ca.readthedocs.io/en/release-1.4/


#### Can I setup multiple Fabric CA servers in cluster mode?
Yes you can. For more details how to do that refer to the [Hyperledger CA documentation](https://hyperledger-fabric-ca.readthedocs.io/en/release-1.4/users-guide.html#setting-up-a-cluster).

#### Why there are two CA servers for a single Membership Service Provider/Ordering Service Provider?
The two CA servers are root CAs and used for different purpose:

- to issue certificates for the Hyperledger fabric identities: users, peers and orderers

- to issue TLS certificates used to establish secure TLS based connections between the Hyperledger fabric nodes (peers, orderers and clients)

#### How to connect to the Certificate Authority (CA) server?
The CA servers are accessible over highly secured mutual TLS connection by using Fabric CA CLI or Fabric CA REST APIs as described [here](https://hyperledger-fabric-ca.readthedocs.io/en/release-1.4/users-guide.html#overview).