---
title: "Build Business Networks"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: Define, deploy and run your business network on a cloud provider of your choice or on premise. Manage and monitor your network life cycle and your infrastructure. Scale, grow and diversify your network to get the best value.
include_footer: true
button: Go To App
button_link: https://app.consortia.io/
---

## Challenge
Building and operating a decentralized business network is a long and complex process. It is essential for your project success to choose the right technology, infrastructure and tools.

## Consortia.io
Simplifies and accelerates the process of managing the lifecycle of your business networks and applications. Consortia.io is based on the industry leading enterprise grade [Hyperledger Fabric](https://www.hyperledger.org/projects/) permissioned distributed ledger technology.

### Why Consortia.io?

#### Cloud Compatibility
Consortia.io is cloud agnostic and can deploy your business network on infrastructure based on any cloud provider. Consortia.io can deploy and run your network nodes on machines hosted on different clouds and/or your own infrastructure. Make your business network distributed and robust.

#### Security
Secure your business network, transactions and data. Consortia.io can deploy and manage your business network on infrastructure owned by you. Your valuable business data stays on your own hardware.

#### Fast Deployment
Focus on implementing your business processes instead on deploying and operating your business network and applications. Consortia.io significantly simplifies the process of building and scaling your business networks. You can quickly build and deploy a new network and scale it later by adding new nodes. Consortia.io will do the heavy lifting to setup a network orchestrator for your infrastructure clusters and deploy your networks and applications on top.

#### Managed
Consortia.io helps you save significant amount of effort and time to build and operate your business networks. You can manage multiple business network members and quickly create a consortium. Consortia.io lets you define environments in order to stage your project from development to production.

#### Automation
You may automate your DevOps workflows, integrate within your system landscape and tooling by using the [Consortia Web APIs]( https://app.consortia.io/api).
