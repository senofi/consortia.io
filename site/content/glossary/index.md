---
title: "Glossary"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: It is important to understand the terminology so that we understand what we mean by each use of a specific term.
include_footer: true
---
<a name="machine"></a>
#### Machine
A machine is a computer or virtual machine based on Linux. Machines can run on your local network or can be hosted on a cloud provider and are used to run your network nodes.

<br><a name="cluster"></a>
#### Cluster
A cluster is a set of machines that are orchestrated by a software like Docker Swarm or Kubernetes (k8s). We use cluster to easily manage the machines and the related infrastructure.

<br><a name="infrastructure"></a>
#### Infrastructure
An infrastructure represents your computational resources (machines and clusters) and is used to run your network and applications.

<br><a name="credential"></a>
#### Credential{#credential}
A credential is usually a user and password or a private key that grants access to a resource such as machine on particular protocol (like SSH).

<br><a name="membership-service-provider-msp"></a>
#### Membership Service Provider (MSP)
This is Hyperledger Fabric term and we use it with the same meaning. You can read further here: https://hyperledger-fabric.readthedocs.io/en/release-1.4/glossary.html#membership-service-provider

<br><a name="consortium"></a>
#### Consortium
A consortium is a set of organizations represented by their Service Providers that form a network they can perform transactions on. You can read further on Hyperledger Fabric's website: https://hyperledger-fabric.readthedocs.io/en/release-1.4/glossary.html#id10


<br><a name="ordering-service-provider-osp"></a>
#### Ordering Service Provider (OSP)
Ordering Service Provider is an organization defined by a specific MSP that explicitly provide ordering nodes and takes part in consortium governance.
Ordering Service is a Hyperledger Fabric term and we use it more or less with the same meaning. You can read further here: https://hyperledger-fabric.readthedocs.io/en/release-1.4/glossary.html#ordering-service
<a name="ca"></a>

<br><a name="certificate-authority-ca"></a>
#### Certificate Authority (CA)
Certificate Authority in general is an entity that issues digital certificates. In our context we use that term to refer to an instance of Hyperledger Fabric Certificate Authority server. For more details read here: https://hyperledger-fabric-ca.readthedocs.io/en/release-1.4/

<br><a name="ordering-service"></a>
#### Ordering Service
This is Hyperledger Fabric term and we use it with the same meaning. You can read further here: https://hyperledger-fabric.readthedocs.io/en/release-1.4/glossary.html#ordering-service

<br><a name="node"></a>
#### Node
A node usually is a software that is essential part of your business network and is running on top of your infrastructure. Nodes could be peers, orderers or CA servers.

<br><a name="channel"></a>
#### Channel
A channel is defined by the participating organizations (defined by their OSP and MSP). It is used to to execute your DLT transactions. Further reading is available on HLF website: https://hyperledger-fabric.readthedocs.io/en/master/channels.html

<br><a name="application"></a>
#### Application
An application is a software that runs on top of your business network utilizing the underlying infrastructure. From business network perspective the applications are called chaincodes and implement the business logic on top of the distributed ledger.
Chaincode applications are consumed by another set of applications that are usually also responsible to provide Web APIs and expose user interface (UI) to your customers.

