---
title: "Add a Membership Service Provider on a channel"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: Distributed Ledger Networks with Consortia
include_footer: true
weight: 1
---

A DLT network evolves and changes over time. This may happen for example when the network participants add/remove peers, fine tune the consensus protocol, add a new MSP to an existing channel and so on.

One of the most important parts of the network lifecycle is to add a new participant on the network (i.e when your business network grows and new business partnerships form).

In the context of HLF based networks such change is technically implemented by adding the new MSP on your existing channel(s). Such operation is quite involving and requires a consent and signature form the current participants (the technical steps are well documented on the [HLF website](https://hyperledger-fabric.readthedocs.io/en/release-1.4/channel_update_tutorial.html)).

Consortia automates and simplifies the process of adding a MSP to your existing channels by using a cross account MSP governance process based on set of chaincodes deployed on a consortia operated HLF network.

Below you may find the steps to add a new MSP (owned and operated by another account) on your existing HLF channel with one of your MSPs being currently the only channel's MSP.

---
**Note**

The following pre-requisites are essential before you continue:
- Your MSP is bootstrapped
- Your channel is bootstrapped and your MSP is participating on the channel
- The joining MSP is bootstrapped (it may be owned by yours or any other consortia account)

---

### 1. Create the channel join request

The first step of the process is to have a join request created by the joining account. For that purpose the joining account will need to know the channel name and the channel owner account id.

The inviting account:
- Navigate to Account -> Tokens and locate your Account token under the Account tokens section
- Send the channel name and your account token to the invitee account (done over a secure out-of-band channel like an e-mail)

The invitee account:
- Navigate to Manage -> Channel Updates
- Click on create channel update request button
- Enter the channel name as received by the inviting account
- Enter the inviting account token (as received by the inviting account) in request processing account field
- Select the MSP that you want to join the channel
- Leave the certificates fields empty

---
**Note**

you may manually enter the certificates of the joining MSP. However if you leave it empty Consortia will fetch the your MSP certificates and automatically push them into your channel join request.

---
- Click on create button

Consortia will create the channel join request and start the process of fetching the invitee MSP certificates. When the certificates are fetched, Consortia will push them to the join request. You may inspect the certificates under the join request details page.

- Send the channel join request id to the inviting account: Manage -> Channel Update -> Channel Join Request -> Request Id (done over a secure out-of-band channel like an e-mail)

At that step of the process the inviting account is responsible to collect the consent from the channel participants and technically deploy the change on the channel (ledger) so that the invitee MSP can become part of that channel.

### 2. Create the channel change

After receiving the channel join request id, the inviting account may create a channel change by following the steps:
- Click on create channel change button under my channel changes section in Manage -> Channel Updates
- Select the channel that the invitee's MSP is joining
- Enter and select the join request id as received by the invitee account
- Click on create button

Consortia will start the process of creating the channel change. During this process, Consortia will fetch the information from the channel join request (the certificates and joining MSP name), create the channel change HLF transaction, pull the channel OSP details, pull the current channel participating MSPs and attach all of that information automatically under your channel change.

You may inspect the channel change details after its status is set to ready: Manage -> Channel Updates -> My Channel Changes

After the inviting account inspects all the channel change transition details, you may start the process of signing it.

---
**Note**

Every channel update transaction requires a set of signatures from the channel's participating MSPs. This process is driven by the channel policies and by default the majority of the MSPs have to sign a channel transaction for a successful commit.

---

### 3. Sign the channel change

Enough MSP signatures should be collected on the channel change transaction before deploying. Inviting account MSP may sign the transaction by:
- Open your channel update transaction details: Manage -> Channel Updates -> My Channel Changes and click on your channel change id
- Click on the sign button next to the MSP under signing membership service providers section

Consortia will trigger the process of signing the channel change transaction with the admin user of the selected MSP. You may come back later to the page to check the signature status. When the transaction is signed the status will be set to signed and you will be able to inspect manually the added MSP signature in the raw transaction field. You may trigger the signing process for every MSP you operate until enough signatures are collected to perform successful transaction commit.

### 4. Deploy the channel change

After collecting enough signatures, the inviting account may deploy the transaction:
- Open your channel update transaction details: Manage -> Channel Updates -> My Channel Changes and click on your channel change id
- Click on the deploy button next to the MSP under signing membership service providers section. You may choose one of the MSPs on the channel change transaction that you currently operate.

Consortia will trigger the deployment of your channel change transaction. After the deployment is successfully performed, Consortia will automatically update the channel topology on the inviting and invited account to include the new MSP. Consortia will automatically set a deployed status to the corresponding channel join request and channel change request. 

Now all involved accounts may start updating/deploying their chaincodes and applications to manage any newly agreed endorsement policy and implement any new business processes involving the newly added MSP on the channel.