---
title: "Deploy Your Applications"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: Distributed Ledger Networks with Consortia
include_footer: true
weight: 1
---

Your applications transact on your DLT network by executing the business logic as implemented in the deployed chaincodes. Your application layer depending on your architecture may be simple or quite complex (when for example you need to integrate with other applications in your system landscape). The HLF chaincode can be accessed and invoked by using the [HLF SDKs](https://hyperledger-fabric.readthedocs.io/en/release-2.0/getting_started.html#hyperledger-fabric-application-sdks).

Let's assume we use a chaincode and a web application that exposes the business logic to our end customers (using a HLF SDK that implements a simple Web UI).

Consortia allows you to deploy your chaincodes/applications from a GIT/Docker repository. In order to ease the deployment you may integrate your private repository or use any public repository.


### 1. Integrate your source code repository (optional)

Here are the steps to integrate your source code repository provider (where your chaincode source code is managed) 
- Navigate to Git Providers page (Account -> Integrations)
- Click on add git provider button
- Provide your provider a name you can easily refer to in the future
- Choose your git provider (in case your chaincode is in you private GitHub repository, you may need to provide access to it by checking the check box in the OAuth2 section) 
- Click on Save button

You git provider may ask to grant access to your repositories. This is needed in order to deploy your chaincode on your peers. You may remove the access anytime by using your git provider management console (in case the access is revoked, you will not be able to deploy your chaincode with Consortia).

After you integrate your git repositories you may proceed with the chaincode deployment. The deployment is a multi step [HLF chaincode deployment](https://hyperledger-fabric.readthedocs.io/en/release-1.4/chaincode4noah.html#chaincode-for-operators) process that is simple and easy to perform/automate with Consortia.

### 2. Install your chaincode

The purpose of this step is to bring your chaincode on your peers. Usually the chaincode runs on peers managed and owned by participating network MSPs (MSPs owned and operated by your partners). The process of bringing the chaincode to all network peers is asynchronous and usually managed out-of-band between the participating partners.

---
**Note**

The steps below are relevant for HLF 1.4.x releases. For HLF 2.x.x releases the process of deploying a chaincode is conceptually different.

---



To install a chaincode on your peers 
- Navigate to Consortia page (Manage -> Applications)
- Click on add chaincode button
- Give your chaincode a unique name (the name will be used to identify uniquely the chaincode on the network)
- Select the programming language your chaincode is implemented in
- Choose the repository of your chaincode (you may manually enter any public repository, i.e. https://bitbucket.org/senofi/demo-trial-contract.git)
- Review you inputs and click the create button
- When in Applications page you may click the install button to install your chaincode on your the peers of your choice. Consortia will navigate to Install chaincode page
- Add the version of the chaincode (i.e. v1)
- Select the repository branch you want to use to install the chaincode from (you may manually enter the branch for public repositories)
- Select and add the peers you want to install the chaincode version on
- Review your inputs and click on the install button to start the chaincode install process. You can find further details on [HLF documentation](https://hyperledger-fabric.readthedocs.io/en/release-1.4/chaincode4noah.html#installing-chaincode)

---
**Note**

You may install multiple version of your chaincode on your peers and later on decide what version to instantiate.

---

---
**Note**

Under the Chaincode Deployment Layout section you may see the list of peers/channels where the selected chaincode version is installed/instantiated and the current status of the install process. Keep in mind you install a chaincode version on peers and instantiate it on channels.

---

After the installment of your chaincode is done, you may proceed with the chaincode deployment process and instantiate it on a channel.

### 3. Instantiate (upgrade) your chaincode

The [HLF chaincode instantiation](https://hyperledger-fabric.readthedocs.io/en/release-1.4/chaincode4noah.html#instantiate) is a DLT transaction that is endorsed by the peers and ordered on the chosen channel through the related OSP.

Perform the following steps to instantiate your chaincode:
- Navigate to Chaincodes page (Manage -> Applications)
- Locate your chaincode and click on Deploy button
- Select the chaincode version and the channel to deploy on
- Provide your endorsement policy and initializing parameters (array of strings between the brackets) 
- Provide you PDC definition in case your chaincode uses [Private Data Collections](https://hyperledger-fabric.readthedocs.io/en/release-1.4/private-data/private-data.html)
- Review your inputs and click Deploy on Channel button (Consortia will automatically navigate to the Chaincode list page)

---
**Note**

Consortia will automatically set the endorsement policy based on the MSPs that are participating on the selected channel. You may always edit the policy if needed.

---

---
**Note**

You may instantiate a single chaincode version at a time. After the first instantiation of the chaincode you may upgrade it due to any reason (i.e. bugfix, endorsement policy upgrade, etc.).

---

---
**Note**

Under the Chaincode Deployment Layout section you may see the list of channels where the selected chaincode version is instantiated.

---

After the chaincode is deployed you may proceed further with the deployment of your application that will consume the chaincode and expose the business logic to your customers.

### 4. Deploy your web application

The HLF chaincode business logic is usually executed using the [HLF provided SDKs](https://hyperledger-fabric.readthedocs.io/en/release-1.4/glossary.html#software-development-kit-sdk). HLF community also provides [best practice design elements](https://hyperledger-fabric.readthedocs.io/en/release-1.4/developapps/designelements.html#application-design-elements) for those applications.

Let's assume you already have followed the best practices and you have your HLF application packaged and available on a docker repository.

Consortia may help you easily deploy your docker packaged web application and connect it to the HLF network nodes.

Perform the following steps to deploy your web application from a docker repository:
- Navigate to Web Applications page (Manage -> Applications)
- Click on the Add Web Application button
- Provide a unique name for your application you can easily recognize
- Provide information on your web application docker registry (repository url, login credentials, web application image name, etc.)
- Select the Web Application MSP (only identity of a MSP that is part of a channel can access the chaincode)
- Select the target machine where your application will run
- Select the access port (internal and external) of your application (for example you may enter internal port 8080 in case your web application listens on that port and external port 80 to expose it to the outside of the cluster)
- Provide any required key/value pairs that will be injected as environment variables to the container of your application
- Review your inputs and click Create button (Consortia will automatically navigate to the Web Applications list page)
- Click on Deploy button of your application to trigger the deployment operation (the status will be set to bootstrapped when the deployment is successful).

---
**Note**

You may deploy a sample chaincode and web application by following the [steps](/docs/getting-started/sample-app/).

---