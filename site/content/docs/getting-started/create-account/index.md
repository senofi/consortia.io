---
title: "Create and Manage a Consortia Account"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: Distributed Ledger Networks with Consortia
include_footer: true
weight: 1
---

The first step before starting to build your Hyperledger Fabric based networks with Consortia is to register and create an account.


### 1. Register a user

To register new Consortia user follow the following steps:
- Navigate to [Consortia login page](https://app.consortia.io)
- Provide a valid e-mail (the provided e-mail is your Consortia unique ID, make sure you always have access to it)
- Type in your password (make sure you remember it)
- Read and accept Consortia [Terms of Use](/terms/)

Consortia will automatically navigate to the account creation section.

### 2. Create an account

You can create an account during new user registration
- On the account creation page type in you account name (you can later on invite users to your account)
- Consortia will set the newly registered user as owner of the account
- Click on the create account button to create the account

Consortia will automatically navigate to your plan selection.

### 3. Select your plan

To complete the registration you have to select a plan.
- Choose your account plan the fits your needs (later on you can upgrade)
- Click on the select plan button
- Read and accept Consortia [Terms of Use](/terms/)
- Depending on the selected plan you may need to provide a credit card
- Click on the button to navigate to Consortia dashboard

### 4. Invite a user to your account

You can invite a user to become part of your account by e-mail. Consortia will send an invitation to the provided e-mail with unique link so that the invited person can accept the invitation.
After clicking on the invitation link, the invitee will be asked to login with existing or register a new consortia user.
- Navigate to Team sub-menu under Account menu on the left hand side
- Click on Invite User button (only the owner of the account can invite new users to join)
- Provider the e-mail of the invitee (make sure it is correct as this will be the unique ID of the invited Consortia user)
- Send the invite (note invitation has an expiration period of 3 days)
- You can monitor the invitation status or send new invitation

### 5. Reset my password

In case you have forgotten your password, you can reset it by providing your Consortia user e-mail (the one used to register)
- Make sure you have access to the e-mail of your Consortia user
- Navigate to [Consortia login page](https://app.consortia.io)
- Click on Forgot Password link
- Type in your Consortia user e-mail (the e-mail used during your user registration)
- Click on Submit button
- Wait for an email from Consortia (check your inbox for any blocked e-mails)
- Click on the link (you will be forwarded to a consortia.io page where you can type in your new password)
- Type in your new password and confirm


### 6. Change my password

To change your password after login:
- Select the drop down user menu from the top right corner
- Click on the user profile option
- Under the change password section, type in your current password, type in your new password and re-type it in the confirmation field
- Click on update button


### 7. Sign Out

To sign out from Consortia:
- Select the drop down user menu from the top right corner
- Click on Sign Out option




Proceed to the next step __[Bring Your Infrastructure](/docs/getting-started/byi/)__