---
title: "Create Your Consortium"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: Distributed Ledger Networks with Consortia
include_footer: true
weight: 1
---

To deploy your DLT applications you need a bootstrapped [consortium](/glossary/#consortium). A HLF consortium is build up from so called [Ordering Service Provider](/glossary/#ordering-service-provider-osp) and [Membership Service Provider](/glossary/#membership-service-provider-msp). When building your own consortium from scratch you have to deploy your MSP/OSP and your [channel](/glossary/#channel). The channel will be use later to deploy your applications and conduct transactions. You may as well just deploy your MSP and [join a channel](/docs/join-channel/) that is part of an existing consortium.

### 1. Deploy your Membership Service Provider (MSP)

Your Membership Service Provider is is an abstracted HLF model of your business organization. MSP defines and governs your organization identity wihtin the consortia it is part of. Consortia enables you easily to model and deploy your MSP

Here are the steps to deploy you MSP
- Navigate to Consortia page (Manage -> Consortia)
- Click on create msp button
- Provide your msp unique name (note this name will uniquely identify your MSP in the consortia it is part of)
- Enter your peers details: unique name, machine where the peer will run, peer endpoint (if different from the machine host) and port
- Enter your MSP Certificate Authority servers details: machine where the CA servers will run, unique name, port, endpoint (if different from the machine host)
- Review you inputs and click the create button
- When in Consortia page you can click the deploy button to trigger the deployment of your MSP (when deployment is finished Consortia will set the status to bootstrapped)

---
**Note**

You may remove, create and deploy your MSP peers anytime after your MSP is bootstrapped. You will have to explicitly trigger the deployment of your MSP peers from Nodes page (Manage -> Nodes).

---

---
**Note**

When the endpoint of your node (CA, peer, orderer) is empty, Consortia will use the node's assigned machine host name and provided port as an endpoint for your node. A valid endpoint is a combination of host and port (i.e. peer.consortia.com:1234).

---

After the deployment of your MSP is done, you may [join a channel](/docs/join-channel/) that is part of an existing consortium or proceed with the next steps to deploy your own consortium and channel.

### 2. Deploy your Ordering Service Provider (OSP)

The [ordering service provider](/glossary/#ordering-service-provider-osp) is essential part of your network. Similar to your MSP it consists of CA servers and nodes called orderers ([further reading on HLF website](https://hyperledger-fabric.readthedocs.io/en/latest/orderer/ordering_service.html)). You need your OSP

Perform the following steps to model and deploy your ordering service provider:
- Navigate to Consortia page (Manage -> Consortia)
- Click on create consortium button (the OSP is the core part of your consortium)
- Provide your osp unique name (note this name will uniquely identify your OSP)
- Enter your ordering nodes details: unique name, machine where the orderer will run, orderer endpoint (if different from the machine host) and port
- Enter your OSP Certificate Authority servers details: machine where the CA servers will run, unique name, port, endpoint (if different from the machine host)
- Select the MSPs that will be part of the consortium (those will be consortium founding members)
- Review you inputs and click the create button (you will be automatically navigated to the Consortia page)
- When in Consortia page you may click the deploy button to trigger the deployment of your OSP and bring up your consortium (when deployment is finished Consortia will set the status to bootstrapped)

---
**Note**

The OSP has its own CA servers that manage the identities of the OSP ordering nodes. It is possible as well to add ordering nodes managed by other OSPs to the ordering service (if required to have mutual governance of the ordering service within the participating consortium organizations)

---

After the deployment of your OSP is done (bootstrapped), you are ready to create and deploy a channel (that will be you ledger).

### 3. Deploy your channel

You DLT applications (chaincode) are deployed on a channel (ledger) that is managed by the participating consortium members. First step is to define and deploy your channel on the consortium OSP. After that, every participating MSP may join its peers on the  channel and deploy the chaincodes.


Follow the steps to define and deploy your channel:

- Navigate to Infrastructure page (Manage -> Channels)
- Click on the button create new channel
- Enter the unique channel name and a channel description for your future reference
- Choose the consortium (OSP) to deploy your channel on
- Choose an originating MSP (that is the MSP that will trigger the channel deployment transaction)
- Add the participating MSPs that will be initially part of the channel (including the originating MSP as selected in the previous step)
- Review you inputs and click the create button (you will be automatically navigated to the Channels list page)
- When in Channels page you can click the deploy button to trigger the deployment of your channel (when deployment is finished Consortia will set the status to bootstrapped)

---
**Note**

You must make sure all channel's related OSP and MSPs are bootstrapped before deploying.

---

---
**Note**

 If Consortia cannot establish SSH connection to the machines of the MSPs and OSP the deploy operation will fail. Make sure your infrastructure is accessible and operational before deploying the channel.

---

---
**Note**

After the channel is deployed you can [join MSPs to the channel](/docs/join-channel/) in order to expand your network.

---

### 4. Join your peers on your channel

Every peer of your HLF usually has to be part of a channel in order to conduct private and confidential transactions (endorse by calling the chaincode application and commit the result on the channel). Hence after bootstrapping the peer you join it on channels in order to make it part of your consortia.


Follow the steps to join your peers on your channel:

- Navigate to Nodes page (Manage -> Nodes)
- Click on the peer you want to be part of your channel (only bootstrapped peers can join channels)
- When on the peer details page under the Channels section click on join on channel button
- Select the channel and click on join peer on channel button

Consortia will trigger the process of joining your peer on the selected channel. When ready Consortia will mark the status of your peer to channel as joined.
It is important to promote to [anchor](https://hyperledger-fabric.readthedocs.io/en/release-1.4/glossary.html?#anchor-peer) at least one of the peers belonging to a MSP (this is mandatory in order to have proper peer discovery in your network).

- Click on the anchor peer button to promote the peer on the channel to anchor (Consortia will trigger a transaction on the channel to mark your peer as anchor)
- When the anchor peer transaction is done consortia will enable the anchor button so that you can later remove the peer as anchor from the channel


Now you are ready to deploy your DLT applications on the network.

---
**Note**

You must make sure your channel and your peer are bootstrapped before joining a peer on a channel.

---

---
**Note**

After the channel is deployed you can [join MSPs to the channel](/docs/join-channel/) in order to expand your network.

---


Proceed to the next step __[Deploy Your Applications](/docs/getting-started/deploy-apps/)__