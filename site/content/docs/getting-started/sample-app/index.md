---
title: "Sample Application Deployment"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: Distributed Ledger Networks with Consortia
include_footer: true
weight: 1
---

After you setup the network and have your HLF network running with a channel and peers, you may follow the bellow's steps to deploy a sample chaincode and web application.
The application demonstrates a DLT transaction that records a case for particular event during a a clinical trial.

The functionality assumes there are multiple parties involved on the clinical trial, who can perform specific steps of the overall process of recording an event at the hospital site to processing that case on the controlling site (i.e. a pharma company).


---
**Note**

The chaincode and web application mentioned below are free to use. However those applications are just samples and should not be used for any production deployment or taken as a reference implementation for a production grade application implementation.

---

---
**Note**

Before you proceed, make sure you have at least 1 MSP with a running peer, a running OSP and a deployed channel. However to complete all steps below you will need at least 2 MSPs. If you perform the steps with a single MSP you will be able only to record a case on the ledger.

---
---
**Note**

Make sure to open the ports so that you can access the web applications from your browser. We will be using in the example below the external ports 8080 and 8090 for the web applications. Make sure they are open or use any other ports you have already opened.

---
---
**Note**

Lets assume you already have [a running consortium](/docs/getting-started/create-consortium/) with the following topology:
- 2 MSPs named __nova__ and __gh__ where:
  -  __gh__ is the MSP controlled by the hospital where the trial is performed (lets name it __GH__)
  -  __nova__ is the MSP controlled by the pharma company responsible for the trial (lets name it __NOVA__)
- a OSP named __novacons__
- a channel named __trial__ with participating MSPs __nova__ and __gh__
- a peer for each of your MSPs
- both of the peers have joined the channel __trial__
- each one of the MSPs peer is deployed as anchor peer
---

### 1. Install your chaincode

The purpose of this step is to bring the chaincode on the peers of __NOVA__ and __GH__.

To install the chaincode on the peers:
- Navigate to Consortia page (Manage -> Applications)
- Click on add chaincode button
- Give your chaincode the unique name __trialcc__
- Select __JAVA__ as the programming language of the chaincode
- Enter and select the following repository
>     https://bitbucket.org/senofi/demo-trial-contract.git
- Review you inputs and click the create button
- When in Applications page you may click the install button to install your chaincode on your the peers of your choice. Consortia will navigate to Install chaincode page
- Add the version of the chaincode: __v1__
- Enter and select the repository branch __develop__
- Select and add the peers of your MSPs (__nova__ and __gh__) to install the chaincode on
- Review your inputs and click on the install button to start the chaincode install process

After the installment of your chaincode is done, you may proceed with the chaincode deployment process and instantiate it on a channel.

---
**Note**

The chaincode implements few simple functions to record a case (on the ledger) based on some very basic information, update the status of the case and query the case data. 

This is very simple implementation to showcase the deployment process. However an access control logic is usually required in the chaincode like who can update the status and who can record a case. For example __GH__ as hospital is the only one who can create a case, __NOVA__ however will be the only one allowed to change the status of the case, etc.

---

### 3. Instantiate your chaincode

Perform the following steps to instantiate your chaincode:
- Navigate to Chaincodes page (Manage -> Applications)
- Locate the __trialcc__ chaincode and click on the deploy button
- Select the chaincode version __v1__
- Select the __trial__ channel to instantiate the chaincode on
- Keep the endorsement policy and initializing parameters as is (default endorsement policy will require peer endorsement by all MSPs on the channel)
- Keep PDC empty (the sample chaincode is not using PDC)
- Review your inputs and click Deploy on Channel button (Consortia will automatically navigate to the Chaincode list page)

After the chaincode is deployed you may proceed further with the deployment of your web application that will transact on the ledger (by calling the chaincode) and expose the business logic to your customers.

### 4. Deploy your web application

The HLF chaincode business logic is usually consumed using the [HLF provided SDKs](https://hyperledger-fabric.readthedocs.io/en/release-1.4/glossary.html#software-development-kit-sdk). HLF community provides as well [best practice design elements](https://hyperledger-fabric.readthedocs.io/en/release-1.4/developapps/designelements.html#application-design-elements) for those applications.

After the chaincode is deployed, you may deploy the corresponding web application that is available on the [dockerhub repository](https://hub.docker.com/orgs/senofiinc/repositories).

You may notice there are multiple web applications (packaged as docker images) on the above repository. Every web application is slightly different depending on the business that will use it. For example __GH__ will use the application to capture the data of a case and record it, whereas __NOVA__ will the application to see the case and update its status.

The web applications above are using the [HLF Java Gateway](https://hyperledger.github.io/fabric-gateway-java/) to transact with the chaincode, expose the functionality over Web APIs and implement simple Vue.js based UI for the end consumer.

Perform the following steps to deploy the web applications (repeat the steps twice for each application):
- Navigate to Web Applications page (Manage -> Applications)
- Click on the add web application button
- Provide a unique name for your applications (__novaweb__ and __ghweb__)
- Provide the docker registry: image __senofiinc/trial-web-nova__ for __novaweb__ and image __senofiinc/trial-web-bostongh__ for __ghweb__
- Select the Web Application MSP: MSP __nova__ for __novaweb__ and MSP __gh__ for __ghweb__
- Select the target machine where your application will run (make sure your machine has enough memory to run the applications)
- Select the ports of your application: __8088__ (already pre-configured) is the internal port for both applications, the external port: __8080__ for __novaweb__ and __8090__ for __ghweb__
- Provide the following environment variables key/value pairs:
  - contractId : __trialcc__
  - channel : __trial__
  - adminUser : <<provide your own and remember it>>
  - adminPassword : <<provide your own and remember it>>
- Review your inputs and click the create button (Consortia will automatically navigate to the Web Applications list page)
- Click on Deploy button of your application to trigger the deployment operation (the status will be set to bootstrapped when the deployment is successful).


### 5. Record a case on the ledger

After both web application above are bootstrapped, your are ready to use them and start recording transactions on the ledger.

To access the applications you may go to Manage -> Applications, where you will find your web applications and the endpoints they may be accessed at.

The __ghweb__ application allows you to record a case providing the patient name and sample description. When the case is recorded the web application uses the HLF Gateway to connect to the peer/orderers to endorse and commit the transaction that contains the case data.

The __novaweb__ application allows you to list the recoded cases and update the status.

