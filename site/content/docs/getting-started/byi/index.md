---
title: "Bring Your Infrastructure"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: Distributed Ledger Networks with Consortia
include_footer: true
weight: 1
---

Consortia provides the tools to model, deploy and manage Hyperledger Fabric based networks. All those networks consist of nodes with various roles like peers and orderers. Those nodes by nature are distributed and managed in decentralized way by the participating partners.

Consortia gives you the freedom and flexibility to choose and bring in your own infrastructure to run your nodes that fits best your needs and budget.

### 1. Provision the machines

Consortia allows you to deploy your nodes on any infrastructure (on cloud or on premise). All you need to do is to provision the [machines](/glossary/#machine) on a provider of you choice (or provision them in house).

There are few rules to follow when provisioning the [infrastructure](/glossary/#infrastructure) 
- Ports are open (TCP and UDP protocols) between the [machines](/glossary/#machine) so they can connect to each other and form a [cluster](/glossary/#cluster)
- Proper users and [credentials](/glossary/#credential) are used to access the machines through ssh (make sure to secure the credentials)
- SSH port 22 is open so that you can manage the machines with Consortia
- Use Ubuntu OS based machines

You can provision machines on different cloud providers and geographical regions. You may want to do that for example if you are planning high network availability (note that the network latency might have impact on your transaction throughput)

### 2. Bring your machines in Consortia

After the machines are provisioned on the providers of your choice you can let Consortia know about them. Consortia is using Docker Swarm to orchestrate the infrastructure and deploy HLF nodes on it. In order to do that Consortia needs to install some open source software and setup the Docker Swarm cluster.

Perform the following steps to let Consortia know about your infrastructure:


- Navigate to Infrastructure page (Manage -> Infrastructure)
- Click on add a machine button
- Capture the name of the machine (unique name you can easily remember and refer to)
- Capture the host of the machine - that is the IP or host name of the machine as per your configuration (consortia should be able to resolve it in order to establish secure SSH connection)
- The SSH port of the machine as per your configuration (consortia will connect over SSH)
- Port range of the open ports your nodes will use. You will be able to select a port for your node that will run on the machine within the provided range.
- Provide user and credential Consortia can use to connect to the machine over SSH (select existing or create new; user/password or private key based credentials are supported).
- Choose a cluster the machine will be part of (create new or select existing).
- Choose the role of the machine in the cluster (a single master is required in the cluster; the master is a manager used to bootstrap the swarm)
- Click on create button to create the machine and consortia will navigate back to the Infrastructure page where you can see your machine

---
**Note**

Consortia will use the provided user to install software (open source) libraries on your machine. If the user is missing permissions Consortia will fail to deploy the cluster (install and setup Docker Swarm).
During deployment process Consortia will create a user named __fabric__ that will be used later on to deploy and manage your HLF nodes and respective artifacts.

---

In case your machine is part of a bootstrapped cluster, you may deploy the machine by clicking the deploy button next to the machine in the list of machines on the Infrastructure page.


### 3. Deploy the clusters

After adding the machines and assigning them to clusters you may deploy the clusters. Your HLF nodes may be deploy only on machines that are bootstrapped and part of a bootstrapped cluster.


You may deploy a cluster by:


- Navigate to Infrastructure page (Manage -> Infrastructure)
- Locate the cluster you want to deploy and click the deploy button at the corner of your cluster header view.

Consortia will automatically set the status of the cluster to bootstrapped when the operation is deployment is successfully.

---
**Note**

You must make sure all the machines in a cluster can resolve each other by their respective hosts/IPs and can connect to each other on any port. This is required in order to deploy and bootstrap successfully a docker swarm cluster.

---

---
**Note**

 If Consortia cannot establish SSH connection to the machines of the cluster, the deploy operation will fail. Make sure the provided machines credentials, hosts and ports are correct and accessible to Consortia.

---

---
**Note**

 You may add and deploy a machine to a bootstrapped cluster anytime. Just add the new machine to the cluster and deploy it by clicking on the deploy button next to the machine in the list of machines on the Infrastructure page.

---




Proceed to the next step __[Create Your Consortium](/docs/getting-started/create-consortium/)__