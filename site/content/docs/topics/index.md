---
title: "Documentation"
date: 2019-10-14T22:20:39-04:00
draft: false
subtitle: Model, deploy and manage Hyperledger Fabric based DLT networks
include_footer: true
aliases:
    /docs/
    /documentation/
---
---
### Getting Started
#### [Setup Consortia Account](/docs/getting-started/create-account/)
#### [Bring Your Infrastructure](/docs/getting-started/byi/)
#### [Create Your Consortium](/docs/getting-started/create-consortium/)
#### [Deploy Your Applications](/docs/getting-started/deploy-apps/)

---
### Consortia Management
#### [Add a Membership Service Provider on a channel](/docs/join-channel/)

---
### Sample Application
#### [Step by step deployment](/docs/getting-started/sample-app)


---
### APIs
#### [Consortia Web APIs](https://app.consortia.io/api)
