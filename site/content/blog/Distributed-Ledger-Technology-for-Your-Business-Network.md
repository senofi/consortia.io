+++
title = "Distributed Ledger Technologies for Your Business Network"
author = "Tsvetan Georgiev"
description = ""
tags = [
    "blockchain",
    "DLT",
]
date = "2020-09-15"
categories = [
    "DLT",
    "Business",
]
keywords = [
    "blockchain",
    "DLT",
    "supply chain",
    "business network"
]
menu = "main"
+++

There are many definitions of Distributed Ledger Technology. Some are technical and sophisticated, some are simple but incomplete. Probably one of the good and accurate definitions of DLT (or Distributed Ledger in short) I have encountered is on the good old Wikipedia:

> A **distributed ledger** (also called a **shared ledger** or **distributed ledger technology** or **DLT**) is a consensus of replicated, shared, and synchronized digital data geographically spread across multiple sites, countries, or institutions.[\[1\]](https://en.wikipedia.org/wiki/Distributed_ledger#cite_note-UKscienceOffice201601-1) Unlike with a [distributed database](https://en.wikipedia.org/wiki/Distributed_database), there is no central administrator.[\[2\]](https://en.wikipedia.org/wiki/Distributed_ledger#cite_note-raiib-2)
> 
> A [peer-to-peer](https://en.wikipedia.org/wiki/Peer-to-peer) network is required as well as [consensus](https://en.wikipedia.org/wiki/Consensus_(computer_science)) algorithms to ensure replication across nodes is undertaken.[\[2\]](https://en.wikipedia.org/wiki/Distributed_ledger#cite_note-raiib-2) One form of distributed ledger design is the [blockchain](https://en.wikipedia.org/wiki/Blockchain) system, which can be either public or private.
> 
> [https://en.wikipedia.org/wiki/Distributed\_ledger](https://en.wikipedia.org/wiki/Distributed_ledger)

Peer to peer networks and distributed database concepts are not new and there are plenty of mature implementations and products out there (i.e. P2P file sharing, modern Distributed DBMS ). Peer-to-peer networks enable distributed application architecture that runs loads and tasks among peers. Those peers are equal participants in the application and they form the so-called peer-to-peer network of nodes. On the other hand distributed database stores data across different physical locations, computers, or interconnected computers. Usually, distributed databases require central control and management.

The key breakthrough with DLT is that there is no need for central administration and the data is distributed across the network participants who operate their own nodes. In other words, there is no central point of control and failure. This concept is extremely important when we use DLT and blockchain technology in the real business world where secure, consistent, and traceable transactions are the key to success.


---

The focus of the article is more on the business application side of DLT instead of the public application side where the token-based blockchains rule (i.e. Bitcoin, Ethereum, etc).

---


## What problems does DLT solve?
    

Many!

Very often we challenge our understanding and use of DLT by asking: What are the problems that only DLT can solve given we have the well established, proven, and robust client-server architecture?

Even though the above question is relevant, it leads us to question why we need DLT and focus on the reasons why DLT will not work, rather than emphasize the potential benefits and value.

So I think the better and more meaningful question to ask is:

What is the DL value compared to solutions based on other technologies?

To answer the above question you need lots of information from real-world implementations that use DLT. Such data of course was pretty hard to get a few years back as almost all of the business implementations were just simple POCs (Proof of Concept). Many of those projects failed or were stopped for various reasons (due to not enough understanding of the technology, hard to find experts in the area, trying to solve the wrong problem, expensive to build up internal knowledge, unstable DLT implementations, lack of tools to manage and deploy the DLT networks, etc).

On the bright side, there are DLT project implementations that have already been deployed and are currently used in production. A good example is the Walmart SCM solution[1]. There are many other production deployments that are still evaluated and I am sure more data will become public in the next few years.

To keep it simple let's focus on few key problem areas that DLT can help solve.

### Trust
    

If you have trust issues in your business partner network DLT is usually the right solution. Have in mind the DLT applications will likely not replace your current systems but rather will integrate and work together. A trust issue doesn’t necessarily mean you do not trust your business partners. It is often a data-related problem where one party receives incorrect information from the other party. In those cases, it is quite difficult for the receiving party to validate the truthfulness of the received data. A good example is when a company pulls a credit report for a customer from their credit bureau partner. The quality of the received data though is not guaranteed and the receiving party has no easy way to validate the consistency of the data. Very often companies implement complex and expensive business processes to mitigate those cases for example by going through manual audits and verification.

So if you struggle to trust the data exchanged with your business partners a DLT based solution must be on your radar.

### Transparency and traceability
    

Very often business partners use their own internal software stacks based on systems and applications provided and maintained by their software vendors. It is difficult to have those systems integrated. Most of the solutions and concepts we use now to make systems talk/exchange data are complex and expensive to implement and maintain (i.e. ETL, Web APIs, message queues, etc.). A single and simple bug in the chain of communication may bring your production system down and significantly harm your business network operations. One of the major problems, in such cases, is that none of your business partners have transparency on the impacted business transactions and data.

Technically speaking, the root cause of the transparency and traceability issue is the missing mechanism of performing deterministic and finite transactions with your partners. A DLT application though where the network nodes are owned and operated by the participating partners delivers full transparency and traceability in real-time on your transactions and data.

In other words, transparency and traceability are given and embedded within the DLT technology itself. The best of all is that your DLT business network will not need expensive and long audits as the consistency and transparency of your data is in place at the moment the transaction is committed.

## Why is DLT Revolutionary?


Data has turned out to be the most valuable asset in the history of mankind. Some of the companies that have achieved above billion or even above trillion of market capitalization deal with data - advertising, marketing, financials.

Almost all data has been siloed in self-governed data centers around the world in legacy systems. There are very little means nowadays on how that data could be shared in a meaningful and trusted way without the risk of exposing too much.

DLT promises to allow a clear way forward for business entities to open up on what they agree to share in a secure way.

As mentioned in the beginning DLT is based on two major concepts: peer-to-peer networks and data (ledger) distribution. From a technical perspective, DLT is a huge breakthrough as it solved the major problem on how to replicate, distribute, and share data consistently between parties in a guaranteed manner without a central control through consensus.

DLT opens the door to simplify, improve, and optimize many business processes across industries. It gives the world a powerful technology that can reshape the way business is done.

# DLT Types


DLT on its own is just a term that describes any technology that would allow for a distributed ledger owned and operated by multiple owners or participants. There are multiple different ways that a DLT could be implemented and the list starts with blockchain, holochain, DAG (Directed Acyclic Graph), and continues with a few others.

For the purpose of this blog post, we would mostly focus on the way the nodes are operated and in that sense, we can distinguish two major types - public and private (permissioned) DLT.

## Public, Unpermissioned DLT

Public DLT networks such as Ethereum are out there and promise the ability to allow anyone willing to use them to record transactions in their ledgers. The public part of the DLT is where all the nodes of the network are hosted either by private entities or the founders of the network (e.g. Ethereum). That does not come for free, though. Every transaction is priced in ETH (Ether Gas), which is the coin used by Ethereum. ETH is a digital currency and as such, it fluctuates based on demand. The Ethereum network nodes run calculations and as of now use the so-called Proof-Of-Work algorithm where a node in the network must find an answer to a complex calculation to earn the trust of the network. That power comes with a cost and every solved calculation is awarded a certain amount of ETH. The problem is that transaction cost can change and usually goes upwards and also the ETH digital currency can spike at any moment as it did in 2018 when it reached $1400 (compared to $350 as of the time this blog post was written).

The processing of a transaction is dependent on the number of nodes in the network and also their workload. As Ethereum is a public network that can run from a few to possibly hundreds or thousands of applications there is no guarantee on how fast a transaction could be processed. It could take a second or an hour. Also, if your network requires hundreds or thousands of transactions per second performance then public DLT is probably not what you should be using as it has been proven that that type of DLT cannot handle that performance load.

When considering a public DLT network you have to ask yourself the following questions:

*   Are performance and scalability important?
    
*   Can my company cope with a variable and possibly rising cost per transaction (often randomly)?
    
*   Am I willing to sacrifice some of the privacy and security aspects of what will be stored in the ledger?
    

## Public, Permissioned DLT

There are also public DLT networks that are also permissioned, meaning that only entities that are approved can be part of the network. Those networks are just a variation of the public DLT that for the purpose of this blog I will not focus on.

## Private, Permissioned DLT

Permissioned DLT is a ledger that only accepts nodes that are trusted and have their network identity known. Those ledgers are referred to also private blockchain/DLT. They use the same concept for storing the transactions and coming to a consensus on whether a transaction could be committed or not, except that you do not need thousands of nodes to prove a transaction is valid. As all the nodes in the network are trusted, only the ability to form consensus based on predefined policy matters. Sometimes the policy may be set with the notion that only 30% of the nodes should agree or on other occasions 51% would be the threshold on whether a transaction is valid and can go in the ledger. Nodes in such networks can be added only with the permission of the existing nodes. One of the most prominent implementations of DLTs of that sort is Hyperledger Fabric, which is an open-source project with a focus on business applications.

Permissioned DLT’s most important benefits are:

*   it is secure - because the data is not only encrypted but also is stored only within the trusted consortium nodes and not shared on public infrastructure all over the world
    
*   inexpensive with predictable costs - the price to run a network is only its maintenance and the costs for running the nodes
    
*   independent - the network is run within the consortium and not dependent on other third-party organization that may be running the system such as Ethereum
    
*   highly performant - a new network does not have to worry about performance and will not face the performance bottlenecks that public blockchains currently have even with higher loads.
    

Of course, along with the benefits come some disadvantages and the main ones are the complexity of maintaining the network and the need for a better organization and a participant’s entry/exit strategy. Of course, those problems could be tackled with proper business processes and the right tools to minimize their impact.

# Should I use DLT?

To simplify: Yes you do! Maybe not now or tomorrow but your business will have to adapt to the realities one way or another (either forced to do it or do nothing and risk to be replaced/become irrelevant).

Whenever you decide to start a project it is important to know if DLT is a viable option to consider. You may not necessarily recognize a problem to start using DLT. I have seen many cases where companies implement workarounds and adjust their business process to overcome particular issues. Many times such solutions become part of their regular operations to a point it is no longer a problem but an expensive business process.

The world of DLT is rather small and misunderstood but promises a lot of potential in the near future. With the rise of the adoption of DLT globally, more and more companies see the benefits of the technology and the number of implementations prove the technology’s significance.

As much as the public DLT could be useful for certain use cases, private DLT is the solution that most enterprises would and should go with. Using a private DLT makes sense because of its security, privacy, and governance models. It is suited perfectly for industries such as supply chain, healthcare, finance, telecommunications.

References

\[1\] [https://www.hyperledger.org/learn/publications/walmart-case-study](https://www.hyperledger.org/learn/publications/walmart-case-study)