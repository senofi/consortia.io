+++
title = "Blockchain DLT Considerations for Supply Chain"
author = "Tsvetan Georgiev"
description = ""
tags = [
    "blockchain",
]
date = "2021-02-23"
categories = [
    "DLT",
    "Business",
]
keywords = [
    "blockchain",
    "DLT",
    "supply chain",
    "business network"
]
menu = "main"
+++

Blockchain DLT based applications and solutions may bring a lot of value to any supply chain in any industry. Blockchain technology has evolved and matured considerably in the past few years. There are a lot of new projects and developments moving the technology forward at a fast pace.

There are business challenges that have to be addressed when implementing blockchain DLT based applications. There is still not enough awareness of the business value of the DLT based solutions. It is hard for many businesses to understand how to adopt the new DLT based business models as usually the thinking of business data and transactions models are very siloed. 

In this article, I will outline some of the technical aspects of what has to be considered when using blockchain DLT. Let’s take a look first at the DLT challenges in the context of supply chains.

## The challenges
Supply Chains are an important part of any industry and their resilience has a significant impact on many businesses and consumers worldwide.  In my previous post [“Supply chain resiliency in difficult times“](/blog/supply-chain-resiliency-in-difficult-times/), I have laid out some crucial challenges in the supply chain context and solutions based on DLT. The supply chain challenges are common across industries when abstracted from the business specifics. From an IT point of view, those issues can be solved by applying generic concepts and adapting them to the business-specific use case. Usually, that goes for any business software where large software vendors provide standard implementations of well-established business processes and those are implemented individually for every client through configuration and/or coding.

I have been involved in multiple projects in different industries (healthcare, construction, food, etc.)  that use blockchain applications to solve business problems. In all of those projects, one of the main challenges was the network/consortium management in production. It is relatively easy to build a consortium with pre-defined partners on the network for demonstration purposes where all nodes are managed on a single infrastructure (managed k8s cluster on a cloud provider). However, things look different when the partners have to be involved and take actions and responsibility for their own nodes and network management. There are many reasons that make consortium management a challenging task, both from a business and technical perspective such as:

* capabilities to self manage the nodes 
* heterogeneous and heavily distributed infrastructure
* cost and incentives
* business model and permission policies 

Often the above challenges are underestimated and overlooked in favor of simplifying the implementation of the DLT network by deploying it on a single infrastructure and introducing a central point of consortium control. This works just fine for POC and even production deployments where a single point of control is already established. However, to harness fully the capabilities of DLT  most of the network nodes will run in multi-controlled and decentralized business and technical models without a single point of control. Therefore a consortium has to implement the best operational model considering current participating entities in the most flexible way to accommodate future changes on the network and business sides. For example, few partners may easily form a consortium using a managed service on a cloud provider or join an existing consortium managed by another party … but all of that comes with a huge trade-off. In those cases, the decentralization aspect of the network is lost, and I would strongly consider a centralized (client/server) implementation instead of using DLT.

## The considerations
There are important considerations to make when planning on how to design and implement your consortia. Those decisions define how scalable the network is and how manageable the onboarding process of adding or removing partners is. Supply Chains are dynamic and their flexibility is crucial to meet the reality. The DLT networks and applications that drive and automate trust between the partners must be flexible enough in order to justify their cost. At the end of the day, we need the software to help us in the best possible way at a meaningful price point.

#### What DLT foundation to use?
This is probably the first and one of the most important decisions to make. I have outlined few areas to consider when choosing the right foundation in [my previous blog](/blog/supply-chain-resiliency-in-difficult-times/).

At the time of writing this article, one of the top blockchain DLT software is maintained under the open-source project Hyperledger Fabric. Based on my experience this is one of the best options to choose considering it has many adoptions in the real world (for more details you can check the [2020 report](https://www.hyperledger.org/learn/publications/hyperledger-annual-report-2020)). There are a few other options to consider - the enterprise releases of the popular Ethereum project or other Hyperledger projects such as Sawtooth.

Here are few important areas to look at when choosing the proper DLT foundation.

* maturity of the project
* adoption levels
* the community behind the project
* the backers of the community (i.e. enterprise technology companies)
* tools availability and ecosystem

#### Where is my data?
Your data is your asset. The location and ownership of the transactional business data are extremely important for any business. It is not just about storing securely your transactions in a database managed by your current cloud provider. It is important to have access to that data for the purpose of extracting or moving it to a different place. For example, multi-cloud deployments are a norm nowadays. Businesses require to be able to move or scale on different providers easily. The last thing you need is to have the data locked-in by a cloud provider with no option of full control. You need to be able to scale your DLT nodes across any infrastructure including moving your nodes and data (without blackout period) anywhere.

#### Do I own and have ultimate control over my nodes?
This is a very important aspect of how efficiently you can manage and operate your nodes. There are many offerings that simplify the nodes' management by providing them as a service (many cloud providers offer managed blockchain services). Using such services is a viable option considering you get an easy-to-use console and automation. However, there are a few important caveats that may totally defeat the benefits of using DLT.

* No native control possible over the nodes. This may mean that not all the features of the chosen DLT may be used. That may significantly limit the capability to add partners to your network.
* The cost is very high. Large businesses may be able to afford to pay a hefty price for their own part of the network. However, most of the Supply Chains include and operate with many small and medium-sized businesses. Those small players cannot justify or assume a high IT cost.
* Upgrades of the nodes to the latest version not supported or comes very late. That may be a showstopper to scaling your supply chain DLT with new partners as they may come in with a different version and require updated applications for various reasons like new capabilities or enhanced security. The option to update to the latest patch or LTS release of your DLT software is essential (which is the case for any software).

#### Can I distribute and scale my nodes across infrastructures/clouds?
Scaling and extending your supply chain DLT network across cloud providers is a must. The old days of locking-in to a single software/service vendor for decades are long gone. The chosen DLT foundation and infrastructure should not limit you in distributing your nodes on different cloud providers. Unfortunately,  many of the large cloud providers have very little to offer in the area of multi-cloud deployments. In my opinion, the future belongs to tools and services that can manage those cross deployments easily.

#### How can I join a consortium?
Joining a consortium or inviting your partners to join your consortium is an essential capability that must be in place. Those features are usually provided by the chosen technology and tools. The ease and cost of use and operation are also very important. For example, partially supported functionalities, or hard and expensive to implement and use ones, may completely defy your consortium viability.

#### How can I take part in consortium management and operations?
Being part of the management and operation of your consortium is not necessarily required. Depending on the agreement between the network participants only specific parties can assume the role and responsibility to define and operate the network (i.e. maintain the permissions, develop and upgrade the applications). However, every consortium should be capable of easily include or exclude a party from the management party set. This can guarantee there is no single point of failure in case the business model changes in the future and consortium topology should adjust accordingly. 

#### What are the benefits and costs?
Depending on the tools and foundation used for your supply chain DLT the cost and time for scaling might differ significantly. Any DLT would make sense if it is economically viable (in fact this is true for any software solution). If scaling requires a project-like investment then the cost may not justify the solution. Have in mind your DLT network is supposed to and will evolve over time. This means the network scaling process is important and its cost should be planned upfront. Even though a large business may assume easily a high cost their smaller partners on the supply chain usually may not be able to do so. 

#### Do I have the right tools?
One of the most important areas to look at is the availability of tools and related ecosystems. Having the right tools to manage and scale your supply chain network is essential. Furthermore having multiple tools to choose from is always better as you will be able to use the best one for your consortium at the best cost (a highly competitive market usually delivers the best value). Of course, a tool should not limit you to replace it anytime in case something better becomes available in the future. For example, a commercial cloud platform may provide great native tooling but completely prevent you from scaling your consortium outside that very same cloud vendor. Such a tool is definitely a poor one to use if scaling your DLT partner network is essential…

## Conclusions
It is important to understand that using Blockchain DLT for your supply chain may require a change in your existing business models. Furthermore, data sharing and distributed transaction models can significantly help you improve your supply chain communication and trust. The DLT technology, tools, and ecosystem have a significant impact on DLT network business value for all participants in the supply chain.
