+++
title = "Supply Chain Resiliency in Difficult Times"
author = "Tsvetan Georgiev"
description = ""
tags = [
    "blockchain",
]
date = "2021-01-09"
categories = [
    "DLT",
    "Business",
]
keywords = [
    "blockchain",
    "DLT",
    "supply chain",
    "business network"
]
menu = "main"
+++

Disruption is one word that could describe the last few months with everything that the coronavirus caused to the economy. Unfortunately, pandemics, epidemics, or natural disasters can happen without a warning and therefore every company should have a contingency plan. A plan is a must but how quickly can a business detect disruption and activate that plan?

According to Gartner<sup>[1]</sup>, one way to minimize the impact on your supply chain is to use “platform, product or plant harmonization”. In the context of manufacturing, for example, a factory can standardize the components it needs for the product it builds making it easier to source it from multiple suppliers. If that is a good idea for your manufacturing process then why not for your supply chain too? What if every one of your partners has the ability to share transactional data so that you are always up to date and have the ability to react swiftly to changes? What if everyone could join your supply chain consortium with ease and start transacting right away?

Let’s take a look at how a consortium of 2 or more companies can organize their business with each other in the context of the supply chain model and how they can transact with each other.

## Old School Paper Trail 
In the digital age, there are still a lot of companies that would do business the old fashioned way - over the phone, fax, email, or even via mail. For one-off orders that may be justified but what about when a company has already established business with a partner and the business is ongoing, with repeatable orders and payments?

The major problems of that approach are the costs involved with the manual work and oversight and the eventual delays in both receiving the goods and receiving payments on both sides. Very often the suppliers work with other businesses down the line of the supply chain. This can go a few levels deep and the receiving company may not necessarily have the visibility down to the lowest level. Furthermore, it is very hard to follow, synchronize, and have a holistic view of the flows of purchase orders, movement of goods, and movement of money. When there are multiple supply tiers it is getting even more complex and hard to have a near-real-time view of the supply chain. This view though is extremely important in order to avoid disruption and impact on the end customers.

As of the time of writing this blog, there are already a few examples of large companies struggling to meet the market demand with their products being out of stock for a long time after the official launch.

Human oversight still cannot be replaced but the right software solution can lower the overall costs and remove the need for mundane work. Let’s explore whether a centralized software solution can help you do that.

## Centralized Software Solution

When it comes to supply chain management there are a plethora of solutions that help small and big companies organize resources and minimize their costs. Most of those solutions focus on maximizing profit by minimizing the expenses for the company related to storage, restocking of goods, etc. Whether it is a cloud solution or in-house deployment, the infrastructure and the data are centralized as they live in the realm of a single business. In the context of a business network that data is siloed. 

If we take a look at a supply chain that involves multiple participants, we often see that all parties in the consortium use disparate solutions - SAP, Oracle, etc. That poses certain challenges such as - how to consolidate the data, which data source is more accurate, and which data source should be trusted in case of discrepancies. 

Often, payment settlement may take much longer than the overall time to ship and receive the goods. In a general supply chain management system a payment may be issued with a delay of 52 days on average<sup>[2]</sup>. Walmart’s new blockchain-based supply management system alleviates that problem and has lowered disputes count from over 70% to less than 3%. For small suppliers that can be a huge business problem and some have to sell heavily discounted invoices in order to survive. Furthermore, financing from the banks for small suppliers is quite hard due to a lack of trust. Usually, the large and well-established companies have easy access to financing but their smaller suppliers down the chain very often have no chance to qualify.

When doing repeated business, automating tasks and consolidating trusted data as early as possible and in the best-case scenario in real-time, is crucial to a dynamic supply chain.  

Of course, systems could be integrated and they could be made to talk directly but that would be a complex task prone to issues, especially when there are more than 2-3 participants in a consortium. Those siloed systems are usually not trusted between the participants as some of them may be purposely tampered with or hacked. 

Regardless of the supply chain nature (global trade, good provenance, etc), there are obvious and well-recognized challenges that the current centralized software systems, processes, and solutions cannot handle well.

## The Supply Chain Challenges

Let’s list some of the current process challenges:

* Manual processes and paper-based documentation
* Difficult asset tracking and traceability
* Transactional errors  
* Fraud due to lack of transparency and real-time tracking of goods
* Counterfeit goods 
* Payment and delivery delays
* Financing for the mid/small suppliers
* Long claim and reconciliation process
* Lack of trust within the supply chain (regardless of whether the transactions are digitalized or not)

Some of the above challenges can be addressed with proper digitalization. 

What we really need in the supply chain is secure and deterministic transactions between the parties governed by a secure consensus achieved automatically on a software level. That way we can achieve mutual trust between all participants on the supply chain and in addition have a real-time view of the state (being it tracking goods, tracing goods provenance, etc.).

Any centralized software solution can only partially address the challenges above and it is almost impossible to provide trust at a transactional level between the parties as it is simply not designed to do that. Those centralized systems have their purpose and certainly will bring value to the supply chain for years to come. 

Can Distributed Ledger Technology (DLT) help us to bridge the gap of the conventional solutions? It certainly can and most definitely can do that in a really good way but not on its own. DLT in this case would be the glue to link the existing centralized systems together and provide the necessary data aggregation and allow for data security and privacy.

## Benefits of using DLT in your supply chain
Using DLT as a medium to link together systems and pour in data from various sources has its undeniable benefits. Here is how the use of DLT can help your supply chain.

### Easy Supplier Onboarding
Supplier onboarding is the process of collecting prospective vendor information, assessing vendor compliance and risks, and integrating them into your platform and supply chain. Put simply, supplier onboarding is about creating streamlined processes to build stronger buyer-vendor relationships that result in improved business outcomes for both parties.

One of the key elements of a successful and low-cost supply chain is the ability to onboard a new supplier with as little effort as possible. If a supplier holds the same standards as you do that could be as easy as adding them to your consortium. With DLT that would be basically adding an organization to your consortium and streamlining the smart contracts.

### Better Systems Integration
As DLT is not centralized it can live on every consortium participant’s network. That would mean that everyone holds their infrastructure and does not depend on a single entity to manage a system or a platform. The majority of the integration work required would be the connection and ability for your existing backend ERP (Enterprise Resource Planning) to talk to the DLT layer.

### Reduced Costs
Eliminating human, prone-to-error mundane tasks, and speeding up the process would bring immense benefits to how you handle your supply chain. Wouldn’t it be nice to focus on business rather than the process? Yes, integration of DLT is an initial cost that cannot be avoided but it is a one-time expense with a rapid ROI rate.

### Improved Data Ownership and Privacy
Who owns the data if everyone has a copy of it? Everyone is responsible for bringing their data to the table. A smart contract would define when a transaction is ready to be saved in the ledger and then everyone will have the same view for that transaction. So the data belongs to the consortium as it should be. Trust and openness are the two major incentives for using DLT.

DLT allows for the data to be available to be read exclusively only by the participants in a transaction. Even if you participate in more than one consortium you will be able to only share the data that you want your partners to see in each consortium.

## How to form a consortium
Now that you are convinced of the benefits of DLT for your supply chain, how could you get started on that? First of all, you will need to get everyone on board. Your partners need to embrace the idea and see the benefits just like you do.

Everyone who is part of the consortium would either want to have their own DLT node or at least have the ability to push data to the DLT layer. The ideal option is that everyone participates on an equal basis and has their own DLT node. That would leave the security and privacy purely to DLT, which is what it was built with from the ground up.

Here is a simple diagram of a consortium that is formed by a manufacturer company having suppliers, logistics partners, distributors, and retailers in its network. 
![DLT Supply Chain](/images/blog/DLT-supply-chain-1.png "DLT in Supply Chain")


## Governance
The DLT network governance is a technical and business consideration that must be defined and established at the very beginning of the process. The business side of the governance needs to choose the best model as this will have an impact on the technical side of the network implementation. Here are a few examples of viable models in the context of permissioned DLT networks (more details on DLT network types [here](/blog/distributed-ledger-technology-for-your-business-network/):

* Cross business governance group: This approach doesn’t require a new business entity. It is mainly used to simplify and speed up the process of implementing DLT network. Usually, the business entities that participate in the network form a group (business and technical committee) of professionals who represent each party. Such a formed group requires consensus between the participants to define the DLT network and its operational aspects. Those groups are usually funded by the involved businesses. Such an approach may be efficient in the short term and yield quick results. Long term though those permissioned DLT networks usually require much better streamlined governance from a legal perspective.

* Governance business entity: This refers to a legal business entity that is created and funded by the participating parties (joint venture). Such an entity is an independent business (where the business participants are founding members) responsible to govern the network and facilitate the technical and architecture part of the network. There are few benefits of such a model as fast decision making, shared risk among participants, streamlined processes, and network deployments. This approach is likely the best model when moving towards a commercialized solution.

* Central organization: A legal entity that fully facilities the network and defines the technical and business aspects of the implementation. Such an entity fully controls the network and the transactional data. This approach for example is applicable in cases where a government or central operator is involved and the process is highly standardized and centrally regulated (including cross border / international regulations)

Every participant may of course be part of multiple consortia governed by a different model. There are cases where data needs to flow between the networks (being it public or private/permissioned) and interoperability is an important aspect of the overall implementation. 

After the governance business model is defined, the involved parties define the network technical topology with a clear definition of the permissions and related policies. Those policies may define for example who the administrators of the network are, what the involved consensus protocol is, what the related endorsement policies are, and others. It is also possible that some of the participating organizations already have nodes (that might be already part of other consortia) and they just have to join them on the new network. This is an important part of the process as it defines how flexible and manageable the newly formed consortium will be in the future when new parties are invited. 

Usually, the technical implementation of the governance model reflects the business model defined in the beginning. It is best to discuss early any anticipated network changes in terms of adding/removing partners as this process has to follow the initially defined policies that may increase the network management effort and complexity.

A network policy may require approvals by the majority of the current participants in order to add a new partner to the network. This means that more than 50% of the current participants have to be involved and take action to provide and record those approvals. This is usually a long and complex process that may take quite some time to be executed and finalized. On the other hand, though it puts the control into the hands of the majority instead of a dedicated centralized single party to manage it end to end.

After the network governance model is defined, the involved participants have to go down on the transactional/application level.

## Technology
One of the most important decisions to make is to choose the right DLT technology foundation. There are multiple implementations on the market that in most cases are open source projects backed up by a community of developers, ready to be used without any licensing cost. However, there are important considerations to make when trying to find the one you want to use.

* Scalability and performance. Those two characteristics are very important as a low DLT transaction throughput may render your application useless. Furthermore, you have to consider that your DLT network will change and evolve over time as it is likely more nodes will be added and more participants will be joining it. This is why scalability should be in place so that your solution can easily handle much more load in the future than what was initially anticipated.

* Architecture and design. Proper architecture and design are important for any software. There are basic concepts to look for like modularity, extensibility, peer-to-peer patterns, etc. It is important as well to consider how you will integrate the new applications in your existing application stack and how well they will fit within your current enterprise architecture. There are usually common standards any software is expected to cover such as security, for example.

* Integration capabilities. Closely inspect how the DLT applications can be integrated with your existing applications and platforms. Integration flexibility is extremely important as one has to expect multiple existing applications may need to synchronize and work smoothly with DLT transactions and data.

* Community, tools, and support. There are great minds behind every great software. The team of developers and companies behind an open-source project is extremely important for the project’s capabilities and success. Long term support and community commitment are equally important. Availability of supporting tools and libraries are vital for any application development. A clear long-term plan and project vision may give you confidence and visibility on what you can expect from the technology in the future.

## Define transactions, data, access rules, integration points
Once, you have a good idea of the desired business functionality (i.e. recording metadata of a diamond and issuing a certificate), you have to define the business transactions, data to store and share on the DLT networks, related business processes, and rules, integration points with existing systems.

* Business processes to be implemented on the DLT network. This step defines the processes that involve the parties on the network. Those processes may include different personas from each participating organization

* Transactions and data to be processed and stored on the DLT network. How those transactions and data will simplify your supply chain model and benefit the participating businesses (e.g. goods shipped, goods received, invoice ready, payment issued)

* A list of personas/actors and what permissions they need to have as well as actions they can perform within the context of a single participant and/or between participants

* Non-trivial cases and what-ifs (e.g. spoiled goods, shipping incidents, IoT sensors readings)

* Integration with existing applications. This consideration has to be made individually by each participant as they may have quite different backend systems in use (e.g. ERP, CRM, etc.)

## Create the consortium and join the parties
Usually, the governance model drives the topology of the DLT network membership. Every organization in the consortium needs an identity that uniquely defines its membership. The membership model is the base that allows you to properly identify the transaction participants and to define the policy of who is handling what. Therefore proper mapping of the business organizations to membership on the DLT network should be done. Note that the membership entities may not necessarily be 1:1 mapped to participating business organizations. There are cases when a single business organization may be represented by multiple unique memberships. Technically speaking the membership can be easily implemented using the PKI paradigm.

Every participant based on its membership has the responsibility to bring up their DLT nodes and configure those to form a real DLT network.

Once the consortium is formed and the DLT network is operational (nodes can recognize and establish a secure peer-to-peer connection) the participants on the network may proceed with the deployment of the DLT applications - the smart contracts.

## Implement and deploy the DLT applications  (smart contracts)
A smart contract is more or less a process defined as a program that interprets the rules of business and makes sure that it handles the business use cases. 

The implementation of a smart contract can be done as a joint effort by the network participants or implemented by a professional third-party vendor based on the business requirements. The selected approach is mainly driven based on the governance model chosen by the participants in the DLT network definition phase. From a technical perspective, the smart contract is a code written in a programming language (e.g. GO, NodeJS, Java, Solidity, etc). This code is deployed on the nodes in the DLT network and its output defines the transaction that will be endorsed and recorded on the ledger. Hence it is important to have the smart contract deployed by the parties who are entitled to endorse the DLT transactions based on the predefined policies (i.e. majority of the participants).

A smart contract lives on the DLT nodes that are part of the consortium. Every member of the network controls its own nodes and what is deployed on them (in the context of permissioned DLT networks). Hence the network participants have to deploy the smart contracts on their nodes in order to be part of the transactions endorsement.

## Integrate with your existing systems
An important step of the solution is to integrate the smart contracts and DLT transactions with the internal systems in use by the participants. It is likely that each participant may use different business applications supplied by different vendors (i.e. SAP, Microsoft, Oracle, etc). The DLT transactions and data are expected to work in alignment with the existing business applications, hence well-defined and implemented integration points are usually required. Those integrations, of course, are individual for each participant and may not be defined, regulated, and agreed on at the consortium level. This implementation effort is usually up to each member to plan and develop. For example, you may want to trigger a workflow or create an incident in your existing CRM platform to notify the responsible business unit in case the temperature inside the goods container goes above the agreed limit. This assumes that there is a successful endorsed DLT transaction that records the event on the network.

Here is a simplified example of what a potential integration with the enterprise application stack may look like.

![DLT Integration](/images/blog/DLT-supply-chain-integration.png "DLT Integration")

## Conclusion
We are living in a very dynamic world. Our supply chain resilience is vital for humanity and we have the technology to improve it further and make it much more robust and flexible.

“All that is great but it sounds like a lot of work”, I hear you say. I would not argue that it is simple. The process is complicated but like everything else, it could be simplified and broken down into a few simple tasks. We, at Consortia, are working on exposing the value of DLT in the business world. We are focusing on making it simple and approachable to form a consortium and join an existing consortium, eliminating two of the most challenging tasks in the process. At the end of the day, it is not the technology that you are interested in but rather what it can do for your business.


#### Resources
[1] https://www.gartner.com/smarterwithgartner/6-strategies-for-a-more-resilient-supply-chain/

[2] https://www.dunforce.com/en/late-payments-are-a-nightmare-for-freelancers/#:~:text=A%20survey%20conducted%20in%20the,the%20economy%20and%20corporate%20activity.